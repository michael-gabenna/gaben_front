
//[1] = Chamada da biblioteca
var mongoose = require('mongoose');

//[2] = url de conexão com o banco mongo.
mongoose.connect('mongodb://localhost/test');

//[3] = instancia do banco
var db = mongoose.connection;

//[4] = mostra erro na conexão.
db.on('error', console.error.bind(console, 'connection error:'));

//[5] = caso a conexão seja aberta.
db.once('open', function(){

})

//[6] = criação do schema
var kittySchema = mongoose.Schema({
	name: string
});

//[7] = cria o model com base no schema.
var kitten = mongoose.model('Kitten', kittySchema);

//[8] = instancia do schema
var silence = new Kitten({name: 'Silence' });

//[9] = faz o log do valor do schema gerado pelo model do mongoose.
console.log(silence.name);

