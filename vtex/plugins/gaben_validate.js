	/* 	stock interativo - 2016 VTEX
		Login B2B - Mabruk
		Autor: Michael Gabenna
		v1.2.3.3
	*/

	//[1] - INFO OBJECT.
		var gabenForm = {
			vtexInfo: [
				{
					store:'espressione:'
				},
				{
					masterData:'espressione.vtexcrm.com'
				}
			],
			formVtex: 'formVtex', 
			store: 'store',
			client: {
				name: 'store', 
				plugin: 'formVtex',
				segment: ['segmento 1','segmento 2', 'segmento 3', 'segmento 4']
			},
			gabenValidate: function(){
				console.log('validar!');
			}			

    	};

	//[2] == MODEL
    	var formModel = {
    		//the secret
			validateFieldName: function($inputId){
				var $input = $inputId;
				var $gabenTheFieldName = $(this).attr(name);
				var $gabentheFieldMessage = $(this).closest('p').siblings('.stk-input-message');

				$($inputId).on("keyup change", function(value, $input, $gabenTheFieldName, $gabentheFieldMessage) {
					var $md_value = this.value;
					var $md_name = this.name;
					var $md_name_v = $md_value;

					//[1]buscar o irmao do input
					$($gabentheFieldMessage).append($md_name_v);
									
						if( $md_name_v.length >= 3){
							console.log('é maior, validado!');
							
							$($gabentheFieldMessage).append('é isso!');
							//$($inputId).append('<span class="stk-input-message-success">' + $gabenTheFieldName + ' Valido!</span>');
							console.log($gabentheFieldMessage);	
						}else{
							console.log('é menor, validado!');
							
							
							//$($inputId).append('<span class="stk-input-message-alert">' + $gabenTheFieldName + ' Social Invalido!</span>');
							//$($gabentheFieldMessage).append('9 pica!');	
						}
						console.log($md_name_v);
						console.log($md_name);
						console.log($md_value);
						console.log($gabenTheFieldName);
				});
			}
		};

    //[3] - instancia de um novo objeto forma do gabenForm.
		var mabruk_form = new Object(gabenForm);
		var mabruk_form_model = new Object(formModel);

	//[003] - busca dentro de um array, um objeto e um valor dentro dele.
	stk-input-message
		//link do marterData.
		mabruk_form.vtexInfo[1].masterData;

	//[4] - busca dentro de um array, um objeto e um valor dentro dele.	
		mabruk_form.gabenValidate();

	//[5] - cria a view dos dados setado no objeto acima.

		var view_form = [

			mabruk_form.formModel['0'].f_name,
			mabruk_form.formModel['1'].f_last_name,
			mabruk_form.formModel['2'].f_email,
			mabruk_form.formModel['3'].f_business_phone,
			mabruk_form.formModel['4'].f_business_name,
			mabruk_form.formModel['5'].f_business_name_fantasy,
			mabruk_form.client.segment,
			mabruk_form.formModel['7'].f_business_type

		];
	//[6] - looping dos selects(off).
		//[6.2] contagem
			var segmento = mabruk_form.client.segment;
			var length = segmento.length,
			    element = null;
			for (var i = 0; i < length; i++) {
			  element = segmento[i];
			  // Do something with element i.
			}

		//[6.1] tentar o .map
			
			var segmentIds = $.map(segmento, function(u) { 
				
				return u; 

			});


		//	
		var i, len, $each_item;		
		for (i = 0, len = mabruk_form.formModel['6'].f_business_segment.length; i < len; ++i) {
			$each_item = mabruk_form.formModel['6'].f_business_segment[i];
			console.log(mabruk_form.formModel['6'].f_business_segment[i]);
			$.each($each_item, function(value){				
				return value;
			})
			//var $item = this.mabruk_form.formModel['6'].f_business_segment[i];
		    //console.log(mabruk_form.formModel['6'].f_business_segment[i]);
		    $("#stk_select_estabelecimento").empty().append('<option value="'+ mabruk_form.formModel['6'].f_business_segment[i] +'">' + mabruk_form.formModel['6'].f_business_segment[i] + '</option>');
		    //console.log($item);
		}	
















    /* cache $element_html */	
		function clientValidate(){
			var labelToCnpj = $("fieldset >.spl_form_line")[0],
				labelToName = $("fieldset >.spl_form_line")[3],
				labelToContact = $("fieldset >.spl_form_line")[1],
				labelToBusiness = $("fieldset >.spl_form_line")[2];
				


				// O.O = 1 |(object 1 - formulario)|
				stkInitForm = {

					firstName: 
						$("#stkFirstName").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkFirstNameValue = $(labelToCnpj).find("p").eq(7).find("input").text(value);

							//console.log(stkFirstNameValue);

						}), //primeiro nome
					lastName: 
						$("#stkLastName").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkLastNameValue = $(labelToCnpj).find("p").eq(14).find("input").text(value);
							//console.log(stkLastNameValue);
							
						}),	//sobrenome
					email: 
						$("#stkEmail").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkEmailValue = $(labelToContact).find("p").eq(3).find("input").text(value);

							//console.log(stkEmailValue);
						}),	//email
					businessPhone: 
						$("#stkTelefoneComercial").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkTelefoneComercialValue = $(labelToContact).find("p").eq(7).find("input").text(value);

							//console.log(stkTelefoneComercialValue);
						}),	
					corporateName: 
						$("#stkRazaoSocial").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkRazaoSocialValue = $(labelToBusiness).find("p").eq(1).find("input").text(value);

							//console.log(stkRazaoSocialValue);
						}),
					tradeName: 
						$("#stkNomeFantasia").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkTradeNameValue = $(labelToBusiness).find("p").eq(6).find("input").text(value);

							//console.log(stkTradeNameValue);
						}),
					corporateDocument: 
						$("#stkCnpj").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkCnjpValue = $(labelToCnpj).find("p").eq(1).find("input").text(value);

							//console.log(stkCnjpValue);
						}),
					stkSegmento: 
						$("#stk-select").on("keyup change", function() {

							var value = this.value; // omit "var" to make it global
							var stkSegmentoValue = $(labelToCnpj).find("p").eq(6).find("input").text(value);

							//console.log(stkSegmentoValue);
						}),		
				}

			this.clientSave = function(){
				var stkStoreName = 'mabrukpresentes'; 
				var stkAccountAppKey = 'vtexappkey-mabrukpresentes-ILVUKY'; 
				var stkAccountAppToken = 'NNNKKAKSJYELTFCVUJULVDPAEAAFXREYQELHEDNQXEZWQDOIRVKUHHSKYMFKAFVSTFVOUUQQWENRESKAMRNBHKRILBJRXDPRIHDKIZOXFUYDTBFGCFXJSTLIONCTLYSS';

					//captacao de informacoes do formulario.
					var stkJsonSaveDadosUser = {
						//campos do formulario.
						firstName: $("#stkFirstName").val(), 	//primeiro nome
						lastName: $("#stkLastName").val(),	//sobrenome
						email: $("#stkEmail").val(),	//email
						businessPhone: $("#stkTelefoneComercial").val(),
						corporateName: $("#stkRazaoSocial").val(),
						tradeName: $("#stkNomeFantasia").val(),
						corporateDocument: $("#stkCnpj").val(),
						stateRegistration: $("#stkinscricaoEstadual").val(),
						addressName: $("#stkEndereco").val(),
						number: $("#stkEnderecoNumero").val(),
						complement: $("stkEnderecoComplemento").val(),
						neighborhood: $("stkEnderecoBairro").val(),
						city: $("stkEnderecoCidade").val(),
						postalCode: $("stkEnderecoCep").val(),		
						state: $("stkEnderecoEstado").val(),
						reference: $("stkEnderecoReferencia").val(),
						stkSegmento: $('#stk-select').val(),
						stkTipoDeEmpresa: $('#stk-select-business').val()
					};

					//$("#stkCnpj").mask("99.999.999/9999-99");
					//$("#stkTelefoneComercial").mask("(99) 9999-9999");	

					var stkUrlSaveDadosUser = 'https://api.vtexcrm.com.br/'+stkStoreName+'/dataentities/CL/documents';
				 
					$.ajax({
						headers: {
							'Accept': 'application/vnd.vtex.ds.v10+json',
							'Content-Type': 'application/json',
							'X-VTEX-API-AppKey': stkAccountAppKey,
							'X-VTEX-API-AppToken': stkAccountAppToken
						},
						data: JSON.stringify(stkJsonSaveDadosUser),
						type: 'POST',
						url: stkUrlSaveDadosUser,
						success: function(data, textStatus, jQxhr){
							console.log(data);
							console.log('carregado com sucesso!');
						  	$("#messageSuccess").removeClass("hide");
						  	$("#messageError").addClass("hide");
						  	$("#stkFirstName").val("");
						  	$("#stkLastName").val("");
				          	$("#stkEmail").val("");          	
						},
						error: function (data, jqXhr, textStatus, errorThrown) {
						  console.log(data);
						  console.log(errorThrown);
						  console.log(jqXhr);
						  console.log(textStatus);
						  console.log('erro ao carregar! A partir da linha 30 do arquivo.');
						  $("#messageError").removeClass("hide");
						  $("#messageSuccess").addClass("hide");
						}
					});
				}

			function validarCNPJ(cnpj) {
				/* test bug */
 				function trim(cnpj) {
				    if(typeof cnpj !== 'string') {
				        throw new Error('only string parameter supported!');
				    }

				    return str.replace(/[^\d]+/g,'');
				
				/* test bug */
			    //cnpj = $(cnpj).replace(/[^\d]+/g,'');
			 
			    if(cnpj == '') return false;
			     
			    if (cnpj.length != 14)
			        return false;
			 
			    // Elimina CNPJs invalidos conhecidos
			    if (cnpj == "00000000000000" || 
			        cnpj == "11111111111111" || 
			        cnpj == "22222222222222" || 
			        cnpj == "33333333333333" || 
			        cnpj == "44444444444444" || 
			        cnpj == "55555555555555" || 
			        cnpj == "66666666666666" || 
			        cnpj == "77777777777777" || 
			        cnpj == "88888888888888" || 
			        cnpj == "99999999999999")
			        return false;
			         
			    // Valida DVs
			    tamanho = cnpj.length - 2
			    numeros = cnpj.substring(0,tamanho);
			    digitos = cnpj.substring(tamanho);
			    soma = 0;
			    pos = tamanho - 7;
			    for (i = tamanho; i >= 1; i--) {
			      soma += numeros.charAt(tamanho - i) * pos--;
			      if (pos < 2)
			            pos = 9;
			    }
			    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			    if (resultado != digitos.charAt(0))
			        return false;
			         
			    tamanho = tamanho + 1;
			    numeros = cnpj.substring(0,tamanho);
			    soma = 0;
			    pos = tamanho - 7;
			    for (i = tamanho; i >= 1; i--) {
			      soma += numeros.charAt(tamanho - i) * pos--;
			      if (pos < 2)
			            pos = 9;
			    }
			    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			    if (resultado != digitos.charAt(1))
			          return false;
			           
			    return true;
			    }
			}
				
				function validadeCNPJ(field, rules, i, options){
					var valido = isCNPJValid(field.val()); //implementar a validação

					//#(470) - verifica se o valor é diferente de valido.
					if (!valido) {
						//internacionalização
					 	return options.allrules.cnpj.alertText;
					}
				}

				// O.O = 2|(instancia do object 1 - formulario)|
				var store_form = Object.create(stkInitForm);
				


				// O.O = 3|(instancia do atributo de object 1 - formulario)|
				var stkIFirstName = $(store_form.firstName);



				// O.O = 4|(instancia do atual atributo de object 1 - formulario)|
				stkFormUp = {
						currentFirstName: function(){
							$("#stkFirstName").on("keyup change", function() {
							   var value = this.value; // omit "var" to make it global
							   $(".stkUpValueThis").text(value);
							   console.log(value);
							});
						}
					};
				function isEmail(email) {
					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					return regex.test(email);
				}
				/* form input value */
				var formFirstNameValue = stkInitForm.firstName.val();
				var formLastNameValue = stkInitForm.lastName.val();

				var formEmailValue = stkInitForm.email.val();
				var formTelefoneComercialValue = stkInitForm.businessPhone.val();

				var formRazaoSocialValue = stkInitForm.corporateName.val();
				var formTradeNameValue = stkInitForm.tradeName.val();

				var formCnpjlValue = stkInitForm.corporateDocument.val();
				var formSegmentolValue = stkInitForm.stkSegmento.val();
      
				/* stk validate email */
				var formEmailValid = isEmail(formEmailValue);


				/* form set message */
					var formMessage = {
						error: "Erro!",
						success: "Sucesso!",
						alert:"Invalido!",
						white: "Branco"
					}

				var stkFormMenssage = Object.create(formMessage);

				/* stk validate cnpj */
				var stkCnpjValue = $("#stkCnpj").val();
				var stkCnpjValueValided = validarCNPJ(stkCnpjValue);


				/* condicoes e validacoes */
				switch(true){

					case(formCnpjlValue === ""):

						console.log("Cnpj em branco!");
						//$(labelToBusiness).find("p").eq(7).empty();
						//$(labelToBusiness).find("p").eq(7).append('<span class="stk-input-message-success">Nome Fantasia Valido!</span>');

							$(labelToCnpj).find("p").eq(3).empty();
							$(labelToCnpj).find("p").eq(3).append('<span class="stk-input-message-error">CNPJ em Branco!</span>');

							break;

					case(stkCnpjValueValided == false):	

						console.log("Cnpj em Invalido!");
						$(labelToCnpj).find("p").eq(3).empty();
						$(labelToCnpj).find("p").eq(3).append('<span class="stk-input-message-alert">CNPJ em Invalido!</span>');			

							break;

					//valida nome em branco(01)
					case(formFirstNameValue === ""):
						$(labelToCnpj).find("p").eq(3).empty();
						$(labelToCnpj).find("p").eq(3).append('<span class="stk-input-message-success">CNPJ Válido!</span>');
						console.log("Primeiro nome em branco! [condicao=01]");
						$(labelToCnpj).find("p").eq(7).empty();
						$(labelToCnpj).find("p").eq(7).append('<span class="stk-input-message-error">' + 'Nome em ' + stkFormMenssage.white + '</span>');

							break;

					//valida nome menor que 3 caracteres(02)
					case(formFirstNameValue < 3):

						$(labelToCnpj).find("p").eq(3).empty();
						$(labelToCnpj).find("p").eq(3).append('<span class="stk-input-message-success">CNPJ Válido!</span>');

						console.log("Primeiro nome menor que 3![condicao=02]");
						$(labelToCnpj).find("p").eq(7).empty();
						$(labelToCnpj).find("p").eq(7).append('<span class="stk-input-message-alert">' + 'Nome ' + stkFormMenssage.alert + '</span>');

							break;

					//valida sobrenome em branco(03)
					case(formLastNameValue === ""):
						$(labelToCnpj).find("p").eq(7).empty();
						$(labelToCnpj).find("p").eq(7).append('<span class="stk-input-message-success">Nome Valido!</span>');

							console.log("Sobrenome em branco.(menor oigual a 3)[condicao=04]");
							$(labelToCnpj).find("p").eq(14).empty();
							$(labelToCnpj).find("p").eq(14).append('<span class="stk-input-message-error">Sobrenome em Branco!</span>');

								break;

					//valida sobrenome invalido(04)	
					case(formLastNameValue < 3):
						console.log("Sobrenome Invalido!(menor oigual a 3)[condicao=05]");
						$(labelToCnpj).find("p").eq(14).empty();
						$(labelToCnpj).find("p").eq(14).append('<span class="stk-input-message-alert">Sobrenome Invalido!</span>');
						
							break;

					//valida email em branco(05)
					case(formEmailValue === ""):

						$(labelToCnpj).find("p").eq(14).empty();
						$(labelToCnpj).find("p").eq(14).append('<span class="stk-input-message-success">Sobrenome Valido!</span>');

							console.log("email está em branco!");
							$(labelToContact).find("p").eq(2).empty();			
							$(labelToContact).find("p").eq(2).append('<span class="stk-input-message-error">E-mail em branco!</span>');
							
								break;

					//valida email invalido(06)
					case(formEmailValid === false):

				 		console.log("email invalido!");
				 		$(labelToContact).find("p").eq(2).empty();
				 		$(labelToContact).find("p").eq(2).append('<span class="stk-input-message-alert">E-mail invalido!</span>');	
						
							break;

					//valida email invalido(07)		
					case(formTelefoneComercialValue === ""):
						
						$(labelToContact).find("p").eq(2).empty();
						$(labelToContact).find("p").eq(2).append('<span class="stk-input-message-success">E-mail Valido!</span>');

							console.log("telefone em branco");

							$(labelToContact).find("p").eq(9).empty();
							$(labelToContact).find("p").eq(9).append('<span class="stk-input-message-error">Telefone em Branco!</span>');

								break;

					//valida email invalido(08)				
					case(formTelefoneComercialValue < 14):
						
						$(labelToContact).find("p").eq(2).empty();
						$(labelToContact).find("p").eq(2).append('<span class="stk-input-message-success">E-mail Valido!</span>');

							console.log("telefone invalido!");

							$(labelToContact).find("p").eq(9).empty();
							$(labelToContact).find("p").eq(9).append('<span class="stk-input-message-error">Telefone Invalido!</span>');

								break;

					case(formRazaoSocialValue === ""):
							
							$(labelToContact).find("p").eq(9).empty();
							$(labelToContact).find("p").eq(9).append('<span class="stk-input-message-success">Telefone Valido!</span>');
							
							console.log("Razão Social em Branco!");
							$(labelToBusiness).find("p").eq(4).empty();
							$(labelToBusiness).find("p").eq(4).append('<span class="stk-input-message-error">Razão Social em Branco!</span>');

								break;

					case(formRazaoSocialValue < 5):	

							console.log("valor minimo de razao social!");
							$(labelToBusiness).find("p").eq(4).empty();
							$(labelToBusiness).find("p").eq(4).append('<span class="stk-input-message-alert">Razão Social Invalido!</span>');

								break;	

					case(formTradeNameValue === ""):
							$(labelToBusiness).find("p").eq(4).empty();
							$(labelToBusiness).find("p").eq(4).append('<span class="stk-input-message-success">Razão Social Valido!</span>');

							console.log("Nome Fantasia em Branco!");
							$(labelToBusiness).find("p").eq(7).empty();
							$(labelToBusiness).find("p").eq(7).append('<span class="stk-input-message-error">Nome fantasia em Branco!</span>');

								break;

					case(formTradeNameValue < 5):

							console.log("Nome Fantasia em Invalido!");
							$(labelToBusiness).find("p").eq(7).empty();
							$(labelToBusiness).find("p").eq(7).append('<span class="stk-input-message-alert">Nome fantasia Invalido!</span>');

								break;			

					case(formSegmentolValue === "white"):
							
							$(labelToBusiness).find("p").eq(7).empty();
							$(labelToBusiness).find("p").eq(7).append('<span class="stk-input-message-success">Nome Fantasia Inválido!</span>');

							console.log("Segmento em branco!");
							$(labelToName).find("p").eq(3).empty();				
							$(labelToName).find("p").eq(3).append('<span class="stk-input-message-error">Segmento em branco!</span>');

								break;
					
					default:

						console.log("Segmento Valido!");

						$(labelToCnpj).find("p").eq(8).empty();
						$(labelToCnpj).find("p").eq(3).append('<span class="stk-input-message-success">Segmento Valido!</span>');

						clientSave();

						console.log("Salvo na Plataforma!");

						$(".pre-login-content").replaceWith('<div class="form-content-success">Pré-cadastrado com sucesso!</div><div class="user_to_login"><a href="/login">Cadastrar uma senha</a></div>');

						break;
				}			
		}

clientValidate();
