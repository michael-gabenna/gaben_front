'use strict';
// simple express server
var express 		= require('express');//[0] - express
var app 			= express();//[1] - app
var bodyParser		= require('body-parser');//[2] - bodyParser
var router 			= express.Router();//[3] - router

//[4]configure app to use BodeParser();
app.use(bodyParser.urlencoded({extend: true}));//
app.use(bodyParser.json());

//[5] - Porta do servidor.
var port = process.env.PORT || 8080;


//[6] - Rotas para a API.
var router = express.Router();

router.use(function(req, res, next){
	console.log('Something is happening.');
	next();
});

//[7] - testa o acesso ao http://localhost/api
router.get('/', function(req, res){
	res.json({mensage: 'hooray! Welcome to our API'});
});


//[8] - registra outra ROTA.
app.use('/api', router);

//[9] - mostra na PORTA.
app.listen(port);


//[10] - log do server.
console.log('Magic Happens on port ' + port);

//[11] - BASE SETUP
var mongoose = require('mongoose');
mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/gaben_front');

var Bear = require('./app/models/bear');

app.use(express.static('public'));

app.get('/', function(req, res) {

    res.sendfile('./public/index.html');

});

app.listen(1000);