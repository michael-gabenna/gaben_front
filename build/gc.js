/* [_GC_] OBJECT */
/* v.1.0.0 */

/*

	[_STEPS_] = 
				[_[0]_]	=	validar se encontra um resultado.
				[_[1]_]	=	validar se encontra um erro.
				[_[2]_]	=	validar se encontra um sucesso.

*/
var gc = {

	pageName: function(name){

		name = name;
		return name;

	},//[0]buscar o nome do endpoint.

	pageUrl = function(url){

		url = url;
		return url;

	},//[1]buscar a url do endpoint.

	pageCategory = function(pageCategory){

		pageCategory = pageCategory;
		return pageCategory;

	},//[2]categoria da página.

	pageEvents = function(event){

		event = event;
		return event;

	},//[3]criar um evento para um endpoint.

	orderConfirmation = function(orders){

		orders = orders;
		return orders;

	}//[4]pagina de confirmação de compra.

}

var gc_basilheus = new Object(gc);

gc_basilheus.pageName();