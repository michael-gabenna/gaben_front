/**
 *
 * Gaben Front - Michael Gabenna
 * @michael-gabenna
 * v0.1.2
 *
 */

/* GC01(GULP==INSTANCIAS==) */
	var gulp 		= 		require('gulp'), 				//chamada do gulp.
		sass 		= 		require('gulp-sass'),			//compilador de sass.
	    //notify 		= 		require("gulp-notify"),			//mostra log in comandline.
	    //bower 		= 		require('gulp-bower'),			//gereciador de plugins para front.
	    watch		= 		require('gulp-watch'),			//indentica alteracoes nos arquivos.
		concat 		= 		require('gulp-concat'), 		//concatena arquivos JS.
		stripDebug 	= 		require('gulp-strip-debug'),	//remove os .log().
		uglify 		= 		require('gulp-uglify'),			//minifica os arquivos.
		rename 		= 		require('gulp-rename'),			//renomea o arquivo com sufix.
		livereload 	= 		require('gulp-livereload'),		//identifica alterações e atualiza.
		npm_coffee	=		require('coffee-script'),  		//biblioteca do npm para coffeescript.
		coffee		=		require('gulp-coffee'), 		//plugin do gulp para coffeescript.
		gutil 		= 		require('gulp-util'),			//relatório de erros para coffeescript.
		concatCss 	= 		require('gulp-concat-css'),		//minify css files.
		jade 		= 		require('gulp-jade'),			//engine to template jade.
		browserSync = 		require('browser-sync'),		//reload in page after change.
		reload      = 		browserSync.reload,				//object on browserSync.
		nodemon 	= 		require('gulp-nodemon');		//recarrega o servidor.		

/* GC02(GULP==CONFIG==) */
	var config = {

		public_css_path:					'./public/css',	//define pasta root dos arquios de sass.
		public_path: 						'./public',	//define pasta root dos arquios de sass.
		sassPath: 							'./resources/sass',	//define pasta root dos arquios de sass.
		bowerDir: 							'./bower_components',	//define pasta root dos plugins gerenciados pelo bower.
		src_jade_path: 						'_src/assets/_jade/**/*.jade', //todos arquivos jade.
		src_js_path: 						'_src/assets/_js/**/*.js', //todos arquivos js.
		src_plugin_path: 					'_src/assets/_js/plugins/**/*.js', // todos arquivos js da pasta plugins.
		src_plugin_coffee_copiled_path: 	'_src/assets/_js/coffee/', // [1]todos arquivos js da pasta plugins.
		src_coffee_path: 					'_src/assets/_coffee/**/*.coffee', //[0] todos arquivos coffee da app.
		dist_coffee_path: 					'_src/assets/_js/coffee/**/*.coffee', // todos arquivos js da pasta plugins.
		src_sass_path: 						'_src/assets/_sass/**/*.sass', //todos arquivos .sass.
		dist_css_path: 						'_dist/assets/css',	//pasta destino do css final.
		dist_jade_path: 					'_dist/assets/jade',//pasta destino dos arquivos convertidos do jade.
		dist_js_path: 						'_dist/assets/js'	//pasta destino do js final.                           
	
	}

/* GC03(GULP==TASK==[01]) */ 	//sass to css
	gulp.task('GC_SASS_CSS', function(sassfiles) {
		gulp.src(config.src_sass_path)
			.pipe(sass().on('error', sass.logError))
			.pipe(concatCss("gaben.min.css"))
			.pipe(gulp.dest(config.public_css_path));
	});

/* GC04(GULP==TASK==[02]) */ 	//coffee to js.
	gulp.task('GC_COFFEE_JS', function(){
		gulp.src(config.src_coffee_path)
			.pipe(watch(config.src_coffee_path))
			.pipe(coffee({bare:true}).on('error', gutil.log))
			.pipe(gulp.dest(config.src_plugin_coffee_copiled_path))
			.on('end', function(){ gutil.log('coffee Done!'); });
	});

/* GC05(GULP==TASK==[03]) */ 	//concat, rename end minify.
	gulp.task('GC_JS_CONCAT_RENAME', function(jsfiles){
		gulp.src([
			config.src_plugin_path, 
			config.dist_coffee_path, 
			config.src_js_path, 
			config.src_plugin_coffee_copiled_path
		])
		    .pipe(concat('gaben.js'))
		    .pipe(rename({suffix: '.min'}))
	        .pipe(uglify())
		    .pipe(gulp.dest(config.dist_js_path))
		    .pipe(gulp.dest(config.public_path + '/js'))
	});

/* GC07(GULP==TASK==[04]) */ 	//jade to html.
	gulp.task('GC_JADE_HTML', function(){
		var YOUR_LOCALS =	{};
		gulp.src(config.src_jade_path)
			.pipe(jade({
				locals: YOUR_LOCALS
			}))
			.pipe(gulp.dest(config.public_path))
	});
	

/* GC08(GULP==TASK==[06]) */ 	//recarregar o browser.
	/*gulp.task('nodemon', function(cb){

		var started = false;

		return nodemon({
			script: 'server.js'
		}).on('start', function(){
			if(!started){
				cb();
				started: true;
			}
		})
	});*/

/* GC09(GULP==TASK==[06]) */ 	//gulp comando padrao.
	gulp.task('default', 
		[
			'GC_SASS_CSS',
			'GC_COFFEE_JS',
			'GC_JS_CONCAT_RENAME',
			'GC_JADE_HTML'
			//'gaben_convert_coffee', 
			//'gaben_convert_css', 
			//'gaben_concat_js_files', 
			//'watch', 
			//'watch-coffee',
			//'gaben_convert_jade',
			//'gaben_copy',
			//'browser-sync'
		], function(){

			//browserSync({server: './public'});

			//gulp.watch(config.src_sass_path, ['GC_SASS_CSS']);
    		//gulp.watch(config.src_jade_path, ['jade-watch']);
		}
	);