var express       = require('express'), 
    http          = require('http');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var routes        = require('./routes/index');
var users         = require('./routes/users');
var dashboard     = require('./routes/dashboard');
var budgets       = require('./routes/budgets');
var systems       = require('./routes/systems');
var perfil        = require('./routes/perfil');
var products      = require('./routes/products');
var node_sass     = require('node-sass');

var app     = express();
var server  = http.createServer(app);

app.configure(function(){

  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');
  app.use(logger('dev'));
  app.use(bodyParser());
  app.use(bodyParser.json());
  app.use('/', routes);  
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(sass.middleware({
      src:__dirname + '/sass',
      dest:__dirname + '/pulic',
      debug: true,
    })
  );
  app.use(express.static(path.join(__dirname, 'public')));
})
app.use('/users', users);
app.use('/dashboard', dashboard);
app.use('/budgets', budgets);
app.use('/systems', systems);
app.use('/perfil', perfil);
app.use('/products', products);