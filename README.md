## DOCUMENTAÇÃO ##
Michael Gabenna
### 1 - Nodemon ###
  = responsável por recarregar o servidor.

### 2 - BrowserSync  ###
  = responsavel por recarregar o browser.

### 3 - Jade  ###
  = responsável por otimizar o conteúdo html.

### 4 - Gulp  ###
  = Automatizador de Tarefas


## Apresentação ##
* gaben-front é uma aplicação padrão para desenvolvimento front-end a partir de boas-praticas do marcado.

## Objetivo ##

* Gerar front-end limpo e que tenha vida útil através de um código que possibilita o gerenciamento e a manutenabilidade.



## Autoria ##
* Gabenna Consulting 2016


## Público ##
* Plataformas e projetos web, que tenham a flexibilidade de alterações no front-end.

## Dever de casa: Node.JS e Tarefas do Gulp! ##
 
1º - git clone 'project'
2º - npm install
3º - rodar o comando 'gulp' na pasta do clone.

## Uso: ##

Editar somente os arquivos .jade.
a conversão será enviada para a pasta ./public

Em breve irei compartilhar as Tarefas do Gulp.

## Email:  pcmicas@gmail.com
## skype: micaelpereiraweb
## linkedin: https://br.linkedin.com/in/michael-gabenna-b35159107


Contribua!